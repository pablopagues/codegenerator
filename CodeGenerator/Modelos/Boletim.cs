﻿using Infrastructure.Domain.Models.Contracts;
using Infrastructure.Domain.Models.SAAR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Models.SAABEInt
{
    public class Boletim : PrimaryKey
    {
        [ForeignKey("DadosColeta")]
        public int IdDadosColeta { get; set; }
        [DisplayName("Dados Coleta")]
        public virtual DadosColeta DadosColeta { get; set; }

        public int IdColetor { get; set; }
        [DisplayName("Coletor")]
        public virtual Responsavel Coletor { get; set; }

        public int IdResponsavel { get; set; }
        [DisplayName("Responsável")]
        public virtual Responsavel Responsavel { get; set; }

        [DisplayName("Recoleta")]
        public virtual String Recoleta { get; set; }

        [DisplayName("Chuvas")]
        public virtual Boolean Chuvas { get; set; }

        [DisplayName("Data Digitação")]
        public virtual DateTime DataLancamento { get; set; }

        [DisplayName("Data Entrada em Laboratorio")]
        public virtual DateTime DataEntradaLaboratorio { get; set; }
        
        [DisplayName("Cloro Residual")]
        public virtual Decimal CloroResidual { get; set; }

        [DisplayName("PH")]
        public virtual Decimal PH { get; set; }

        [DisplayName("Cor")]
        public virtual Decimal Cor { get; set; }

        [DisplayName("Fluor")]
        public virtual Decimal Fluor { get; set; }

        [DisplayName("Turbidez")]
        public virtual Decimal Turbidez { get; set; }

        [DisplayName("Coliformes Totais")]
        public virtual String ColiformesTotais { get; set; }

        [DisplayName("Coliformes Termotolerantes")]
        public virtual String ColiformesTermotolerantes { get; set; }

        [DisplayName("Colônia Heterotrofica")]
        public virtual String ColoniaHeterotrofica { get; set; }

        [DisplayName("Colônia Fora do Padrão")]
        public virtual Boolean ColoniaForaPadrao { get; set; }

        [DisplayName("Conclusão")]
        public virtual ICollection<Conclusao> Conclusoes { get; set; }

        [DisplayName("Observação")]
        public virtual String Observacao { get; set; }

        [DisplayName("Causas")]
        public virtual String Causas { get; set; }

        [DisplayName("Ações")]
        public virtual String Acoes { get; set; }

        [DisplayName("Liberado")]
        public virtual Boolean Liberado { get; set; }

        public int IdManancial { get; set; }
        [DisplayName("Manancial")]
        public virtual Manancial Manancial { get; set; }

        [DisplayName("Numero Boletim")]
        public virtual String Numero { get; set; }

    }
}
