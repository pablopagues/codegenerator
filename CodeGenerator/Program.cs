﻿using CodeGenerator.ClassesGeradoras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Insira o nome do model:");
            String model = Console.ReadLine();

            //usuario digita o modelo a ser usado
            GeraCodigo gerador = new GeraCodigo();
            String resultado = gerador.GerarController(model);

            Console.Write("O modelo gerado foi:" + model);
            Console.WriteLine(" Resultado: " + resultado);

            GeraViews geradorviews = new GeraViews();
            String resultadoviews = geradorviews.GerarViews(model);

            Console.WriteLine(" Geradas as views para o modelo: " + resultadoviews);
            
            Console.ReadKey();
        }
    }
}
