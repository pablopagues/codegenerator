﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodeGenerator.ClassesGeradoras
{
    public class GeraCodigo
    {
        private String folder = "";

        public String GerarController(String model)
        {
            //crio a pasta das views
            folder = Path.GetFullPath(@"..\\..\\ControllersGerados\\");
            //folder = @"C:\temp\Code\CodeGenerator\CodeGenerator\ControllersGerados\";

            //pego todas as propriedades do model
            List<ModelRegister> lstProperties = lstProperties = ModelProperties.getModelProperties(model);

            //gera o codigo do controller para o modelo
            String retornoController = geraController(lstProperties, model);

            //gera o codigo da viewmodel
            String retornoViewModel = geraViewModel(lstProperties, model);

            //gera o codigo do mapper
            String retornoMapper = geraMapper(lstProperties, model);

            //gera o codigo do profile
            String retornoProfile = geraProfile(lstProperties, model);

            return Environment.NewLine + "Controller: " + retornoController + Environment.NewLine + "ViewModel: " + retornoViewModel + Environment.NewLine + "Mapper: " + retornoMapper + Environment.NewLine + "Profile: " + retornoProfile;
        }

        /// <summary>
        /// metodo que gera o codigo para um profile
        /// </summary>
        /// <param name="lstProperties"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraProfile(List<ModelRegister> lstProperties, string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"ViewModels\Mappers\Profiles\" + model + "Profile.cs");
                
                writer.WriteLine("using Infrastructure.Domain.Models;");
                writer.WriteLine("using System;");
                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("using System.Linq;");
                writer.WriteLine("using System.Web;");
                writer.WriteLine("using AutoMapper;");
                writer.WriteLine("");
                writer.WriteLine("namespace Sistema.ViewModels.Mappers.Profiles");
                writer.WriteLine("{");
                writer.WriteLine("    public class " + model + "Profile : Profile");
                writer.WriteLine("    {");
                writer.WriteLine("        protected override void Configure()");
                writer.WriteLine("        {");
                writer.WriteLine("            Mapper.CreateMap<" + model + ", " + model + "ViewModel>();");
                
                //se tiver relacionamento coloco no ignore
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                        writer.WriteLine("                .ForMember(d => d." + reg.Tipo + ", x => x.Ignore());");    
                }

                //TODO: tratar quando tem relacionamento
                //writer.WriteLine("                .ForMember(d=>d.Nome, x=>x.MapFrom(s=>s.professor.Nome))");
                //writer.WriteLine("                .ForMember(d => d.email, x => x.MapFrom(s => s.professor.email))");                

                writer.WriteLine("");
                writer.WriteLine("            Mapper.CreateMap<" + model + "ViewModel, " + model + ">();");

                //se tiver relacionamento coloco no ignore
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                        writer.WriteLine("                .ForMember(d => d." + reg.Tipo + ", x => x.Ignore());");
                }

                writer.WriteLine("        }");
                writer.WriteLine("    }");
                writer.WriteLine("}");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// metodo que gera o mapper para manipular o intercambio viewmodel=>model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraMapper(List<ModelRegister> lstProperties, string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"ViewModels\Mappers\" + model + "Mapper.cs");

                writer.WriteLine("using System;");
                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("using System.Linq;");
                writer.WriteLine("using System.Web;");
                writer.WriteLine("using Infrastructure.Domain.Repositories;");
                writer.WriteLine("using Infrastructure.Domain.Models;");
                writer.WriteLine("using Infrastructure.Domain.Repositories.MappersRepository;");
                writer.WriteLine("using System.Web.Mvc;");
                writer.WriteLine("");
                writer.WriteLine("namespace Sistema.ViewModels.Mappers");
                writer.WriteLine("{");
                writer.WriteLine("");
                writer.WriteLine("    public class " + model + "ViewModelMapper : MapeadorViewModel<" + model + "ViewModel, " + model + ">");
                writer.WriteLine("    {");
                writer.WriteLine("");
                writer.WriteLine("        public override " + model + "ViewModel exportaViewModel(" + model + " model)");
                writer.WriteLine("        {");
                writer.WriteLine("            " + model + "ViewModel viewmodel = base.exportaViewModel(model);");

                //para cada relacionamento carrego os dados para os selectlist
                Boolean temUm = false;
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        if (!temUm)
                        {
                            writer.WriteLine("            GenericRepository genericRepositorio = new GenericRepository();");
                        }
                        temUm = true;
                        writer.WriteLine("            viewmodel." + reg.Tipo + " = genericRepositorio.Tudo<" + reg.Tipo + ">().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();");
                        writer.WriteLine("            if (model." + reg.Tipo + " != null)");
                        writer.WriteLine("                viewmodel.id" + reg.Tipo + " = model." + reg.Tipo + ".id;");
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("            return viewmodel;");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        public override " + model + " importaViewModel(" + model + "ViewModel viewmodel)");
                writer.WriteLine("        {");
                writer.WriteLine("            " + model + " model = base.importaViewModel(viewmodel);");
                writer.WriteLine("            return model;");
                writer.WriteLine("        }");
                writer.WriteLine("    }");
                writer.WriteLine("}");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {
                return "False";
            }            
        }

        /// <summary>
        /// rotina que gera a viewmodel para um modelo
        /// </summary>
        /// <param name="lstProperties"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private String geraViewModel(List<ModelRegister> lstProperties, string model)
        {
            try
            {

                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"ViewModels\" + model + "ViewModel.cs");

                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("using System.ComponentModel;");
                writer.WriteLine("using System.ComponentModel.DataAnnotations;");
                writer.WriteLine("");
                writer.WriteLine("namespace Sistema.ViewModels");
                writer.WriteLine("{");
                writer.WriteLine("    public class " + model + "ViewModel");
                writer.WriteLine("    {");
                writer.WriteLine("        //public " + model + " " + model.ToLower() + " { get; set; }");
                writer.WriteLine("        public int id { get; set; }");
                writer.WriteLine("");

                foreach (var reg in lstProperties)
                {
                    //tratando os relacionamentos
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        writer.WriteLine("        public int id" + reg.Tipo + " { get; set; }");
                        writer.WriteLine("        [DisplayName(\"" + reg.Nome + "\")]");
                        writer.WriteLine("        public IList<SelectListItem> " + reg.Tipo + " { get; set; }");
                    }
                    else
                    {
                        writer.WriteLine("        [DisplayName(\"" + reg.Nome + "\")]");
                        writer.WriteLine("        public " + reg.Tipo + " " + reg.Nome + " { get; set; }");
                    }

                }

                writer.WriteLine("");
                writer.WriteLine("    }");
                writer.WriteLine("}");
                writer.WriteLine("");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True ";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        /// <summary>
        /// gera o codigo do controller
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private String geraController(List<ModelRegister> lstProperties, String model)
        {
            try
            {

                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + model + "Controller.cs");

                writer.WriteLine("using Sistema.ViewModels;");
                writer.WriteLine("using Sistema.ViewModels.Mappers;");
                writer.WriteLine("using Infrastructure.Domain.Base.GenericRepository;");
                writer.WriteLine("using Infrastructure.Domain.Models.Sistema;");
                writer.WriteLine("using Sistema.Controllers;");
                writer.WriteLine("using Sistema.Helpers.Attributes;");
                writer.WriteLine("using Sistema.Helpers.DataTable;");
                writer.WriteLine("using System;");
                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("using System.Linq;");
                writer.WriteLine("using System.Web;");
                writer.WriteLine("using System.Web.Mvc;");
                writer.WriteLine("");
                writer.WriteLine("namespace Sistema.Controllers");
                writer.WriteLine("{");
                writer.WriteLine("    [AuthorizeRoles(\"Administrador\")]");
                writer.WriteLine("    public class " + model + "Controller : FeatureController");
                writer.WriteLine("    {");
                writer.WriteLine("");
                writer.WriteLine("        private IGenericRepository genericRepositorio;");
                writer.WriteLine("");
                writer.WriteLine("        private DataTable<" + model + "> dataTable = new DataTable<" + model + ">();");
                writer.WriteLine("");
                writer.WriteLine("        public " + model + "Controller(IGenericRepository generic_Repositorio)");
                writer.WriteLine("        {");
                writer.WriteLine("            this.genericRepositorio = generic_Repositorio;");
                writer.WriteLine("");
                writer.WriteLine("            dataTable");

                writer.WriteLine("                .addColumn(x => x.id)");
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        writer.WriteLine("                .addColumn(x => x." + reg.Tipo + ".Nome)");
                    }
                    else
                    {
                        writer.WriteLine("                .addColumn(x => x." + reg.Nome + ")");
                    }
                    
                }
                writer.WriteLine("                .drawColumnFilter();");

                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        [HttpPost]");
                writer.WriteLine("        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)");
                writer.WriteLine("        {");
                writer.WriteLine("            return dataTable.getDataTableResult(ControllerContext, genericRepositorio.Tudo<" + model + ">().AsQueryable(), dataTablePostData);");
                writer.WriteLine("        }");
                writer.WriteLine("        ");
                writer.WriteLine("        //");
                writer.WriteLine("        // GET: /" + model + "/");
                writer.WriteLine("");
                writer.WriteLine("        public ActionResult Index()");
                writer.WriteLine("        {");
                writer.WriteLine("            return View(dataTable.renderDataTable());");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // GET: /" + model + "/Details/5");
                writer.WriteLine("        public ActionResult Details(int id)");
                writer.WriteLine("        {");
                writer.WriteLine("            return View(genericRepositorio.EncontrarPorID<" + model + ">(id));");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // GET: /Usuarios/Create");
                writer.WriteLine("        public ActionResult Create()");
                writer.WriteLine("        {");
                writer.WriteLine("            " + model + "ViewModelMapper mapper = new " + model + "ViewModelMapper();");
                writer.WriteLine("            return View(mapper.exportaViewModel(new " + model + "()));");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // POST: /" + model + "/Create");
                writer.WriteLine("        [HttpPost]");
                writer.WriteLine("        public ActionResult Create(" + model + "ViewModel item)");
                writer.WriteLine("        {");
                writer.WriteLine("            try");
                writer.WriteLine("            {");
                writer.WriteLine("");
                writer.WriteLine("                if (ModelState.IsValid)");
                writer.WriteLine("                {");
                writer.WriteLine("                    genericRepositorio.UnitOfWork.BeginTransaction();");
                writer.WriteLine("                    " + model + "ViewModelMapper mapper = new " + model + "ViewModelMapper();");
                writer.WriteLine("                    " + model + " " + model + " = mapper.importaViewModel(item);");
                writer.WriteLine("");

                //tratando os relacionamentos
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        writer.WriteLine("                    " + model + "." + reg.Tipo + " = genericRepositorio.EncontrarPor<" + reg.Tipo + ">(x => x.id == item.id" + reg.Tipo + ").FirstOrDefault();");
                        writer.WriteLine("                    " + model + ".Id" + reg.Tipo + " = " + model + "." + reg.Tipo + ".id;");
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("                    genericRepositorio.Adicionar<" + model + ">(" + model + ");");
                writer.WriteLine("                    genericRepositorio.UnitOfWork.CommitTransaction();");
                writer.WriteLine("");
                writer.WriteLine("                    informaSucesso(\"Registro salvo com sucesso!\");");
                writer.WriteLine("                    return RedirectToAction(\"Index\");");
                writer.WriteLine("                }");
                writer.WriteLine("                else");
                writer.WriteLine("                {");
                writer.WriteLine("                    return View(item);");
                writer.WriteLine("                }");
                writer.WriteLine("            }");
                writer.WriteLine("            catch (Exception e)");
                writer.WriteLine("            {");
                writer.WriteLine("                informaErro(e.Message);");
                writer.WriteLine("                return RedirectToAction(\"Index\");");
                writer.WriteLine("            }");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // GET: /" + model + "/Edit/5");
                writer.WriteLine("        public ActionResult Edit(int id)");
                writer.WriteLine("        {");
                writer.WriteLine("            try");
                writer.WriteLine("            {");
                writer.WriteLine("                " + model + "ViewModelMapper mapper = new " + model + "ViewModelMapper();");
                writer.WriteLine("");
                
                //tratando os relacionamentos
                string relacionamentos = "";
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        relacionamentos += "p => p." + reg.Tipo;
                    }
                }
                writer.WriteLine("                " + model + " u = genericRepositorio.EncontrarPor<" + model + ">(" + relacionamentos + ").Where(p => p.id == id).FirstOrDefault();");
                
                writer.WriteLine("                return View(mapper.exportaViewModel(u));");
                writer.WriteLine("            }");
                writer.WriteLine("            catch (Exception e)");
                writer.WriteLine("            {");
                writer.WriteLine("                informaErro(e.Message);");
                writer.WriteLine("                return RedirectToAction(\"Index\");");
                writer.WriteLine("            }");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // POST: /" + model + "/Edit/5");
                writer.WriteLine("        [HttpPost]");
                writer.WriteLine("        public ActionResult Edit(int id, " + model + "ViewModel item)");
                writer.WriteLine("        {");
                writer.WriteLine("            try");
                writer.WriteLine("            {");
                writer.WriteLine("                if (ModelState.IsValid)");
                writer.WriteLine("                {");
                writer.WriteLine("                    " + model + "ViewModelMapper mapper = new " + model + "ViewModelMapper();");
                writer.WriteLine("                    " + model + " " + model + " = mapper.importaViewModel(item);");
                writer.WriteLine("");

                //tratando os relacionamentos
                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        writer.WriteLine("                    " + model + "." + reg.Tipo + " = genericRepositorio.EncontrarPor<" + reg.Tipo + ">(x => x.id == item.id" + reg.Tipo + ").FirstOrDefault();");
                        writer.WriteLine("                    " + model + ".Id" + reg.Tipo + " = " + model + "." + reg.Tipo + ".id;");
                    }
                }

                writer.WriteLine("                    genericRepositorio.UnitOfWork.BeginTransaction();");
                writer.WriteLine("");
                writer.WriteLine("                    genericRepositorio.Atualizar<" + model + ">(" + model + ");");
                writer.WriteLine("                    genericRepositorio.UnitOfWork.CommitTransaction();");
                writer.WriteLine("");
                writer.WriteLine("                    informaSucesso(\"Registro salvo com sucesso!\");");
                writer.WriteLine("                    return RedirectToAction(\"Index\");");
                writer.WriteLine("                }");
                writer.WriteLine("                else");
                writer.WriteLine("                {");
                writer.WriteLine("");
                writer.WriteLine("                    return View(item);");
                writer.WriteLine("                }");
                writer.WriteLine("            }");
                writer.WriteLine("            catch (Exception e)");
                writer.WriteLine("            {");
                writer.WriteLine("                informaErro(e.Message);");
                writer.WriteLine("                return RedirectToAction(\"Index\");");
                writer.WriteLine("            }");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // GET: /" + model + "/Delete/5");
                writer.WriteLine("        public ActionResult Delete(int id)");
                writer.WriteLine("        {");
                writer.WriteLine("            return View(genericRepositorio.EncontrarPorID<" + model + ">(id));");
                writer.WriteLine("        }");
                writer.WriteLine("");
                writer.WriteLine("        //");
                writer.WriteLine("        // POST: /" + model + "/Delete/5");
                writer.WriteLine("");
                writer.WriteLine("        [HttpPost]");
                writer.WriteLine("        public ActionResult Delete(int id, " + model + " u)");
                writer.WriteLine("        {");
                writer.WriteLine("            try");
                writer.WriteLine("            {");
                writer.WriteLine("                genericRepositorio.UnitOfWork.BeginTransaction();");
                writer.WriteLine("                genericRepositorio.Deletar<" + model + ">(genericRepositorio.EncontrarPorID<" + model + ">(id));");
                writer.WriteLine("                genericRepositorio.UnitOfWork.CommitTransaction();");
                writer.WriteLine("                informaSucesso(\"Registro apagado com sucesso!\");");
                writer.WriteLine("                return RedirectToAction(\"Index\");");
                writer.WriteLine("            }");
                writer.WriteLine("            catch (Exception e)");
                writer.WriteLine("            {");
                writer.WriteLine("                informaErro(e.Message);");
                writer.WriteLine("                return RedirectToAction(\"Index\");");
                writer.WriteLine("            }");
                writer.WriteLine("        }");
                writer.WriteLine("    }");
                writer.WriteLine("}");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }
    }
}

