﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CodeGenerator.ClassesGeradoras
{
    public class ModelRegister
    {
        public String Tipo { get; set; }
        public String Nome { get; set; }
    }

    public static class ModelProperties
    {

        /// <summary>
        /// escaneia o model para obter todas as suas properties e tipos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<ModelRegister> getModelProperties(String model)
        {
            //Declaração do reader com o arquivo modelo a ser lido e tratado
            string exactPath = Path.GetFullPath(@"..\\..\\Modelos\\" + model + ".cs");

            StreamReader objReader = new StreamReader(exactPath);

            String sLine = "";
            List<ModelRegister> lstProperties = new List<ModelRegister>();

            //carrega todos as propriedades do modelo numa lista
            while (sLine != null)
            {
                sLine = objReader.ReadLine();
                ModelRegister reg = new ModelRegister();
                if (sLine != null)
                {
                    if (sLine.IndexOf("virtual") > 0)
                    {
                        String[] campos = sLine.Split();
                        reg.Tipo = campos[10];
                        reg.Nome = campos[11];
                        lstProperties.Add(reg);
                    }
                }
            }
            objReader.Close();
            return lstProperties;
        }
    
    }


}
