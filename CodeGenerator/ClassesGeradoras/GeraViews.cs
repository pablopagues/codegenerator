﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.ClassesGeradoras
{
    public class GeraViews
    {

        private String folder = "";

        public String GerarViews(String model)
        {
            try
            {
                //crio a pasta das views
                folder = Path.GetFullPath(@"..\\..\\ViewsGeradas\\" + model);
                //folder = @"C:\temp\Code\CodeGenerator\CodeGenerator\ViewsGeradas\" + model;
                if (!Directory.Exists(folder))
                {
                    //Criamos um com o nome folder
                    Directory.CreateDirectory(folder);
                }

                //pego todas as propriedades do model
                List<ModelRegister> lstProperties = lstProperties = ModelProperties.getModelProperties(model);

                //gera o codigo da view para o modelo
                String retornoViewIndex = geraIndex(model);

                //gera o codigo da view para o modelo
                String retornoViewCreate = geraCreate(model);

                //gera o codigo da view para o modelo
                String retornoViewEdit = geraEdit(model);

                //gera o codigo da view para o modelo
                String retornoViewDetails = geraDetails(model);

                //gera o codigo da view para o modelo
                String retornoViewDelete = geraDelete(model);

                //gera o codigo da view para o modelo
                String retornoView_CamposDetalhe = gera_CamposDetalhe(model, lstProperties);

                //gera o codigo da view para o modelo
                String retornoView_CamposFormulario = gera_CamposFormulario(model, lstProperties);

                return Environment.NewLine + "ViewIndex: " + retornoViewIndex + Environment.NewLine + "ViewCreate: " + retornoViewCreate + Environment.NewLine + "ViewEdit: " + retornoViewEdit + Environment.NewLine + "ViewDestails: " + retornoViewDetails + Environment.NewLine + "ViewDelete: " + retornoViewDelete + Environment.NewLine + "ViewCamposDetalhe: " + retornoView_CamposDetalhe + Environment.NewLine + "ViewCamposFormularios: " + retornoView_CamposFormulario; 
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view index para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraIndex(string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\Index.cshtml");

                writer.WriteLine("@model DataTableViewData<" + model + ">");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"inbox-head bg-light\">");
                writer.WriteLine("    <h3><i class=\"glyphicon glyphicon-list\"></i> " + model + "</h3>");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("@Html.DrawDataTable(Model)");
                writer.WriteLine("<div class=\"col-lg-12 well\">");
                writer.WriteLine("    @Html.LinkTo(\"Create\", \"btn btn-success\", \"Adicionar Novo " + model + "\", \"Novo " + model + "\", \"glyphicon glyphicon-plus\")");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("@section scripts {");
                writer.WriteLine("    @Html.DrawDataTableHeader(ViewContext, Model, 5, DataTableSortDirection.Descending)");
                writer.WriteLine("}");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view create para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraCreate(string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\Create.cshtml");

                writer.WriteLine("@model " + model + "ViewModel");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"inbox-head bg-success\">");
                writer.WriteLine("    <h3><i class=\"glyphicon glyphicon-plus\"></i> Novo " + model + "</h3>");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"form-horizontal\">");
                writer.WriteLine("    @using (Html.BeginForm())");
                writer.WriteLine("    {");
                writer.WriteLine("        @Html.Partial(\"_CamposFormulario\")");
                writer.WriteLine("");
                writer.WriteLine("        <div class=\"col-lg-12 well\">");
                writer.WriteLine("            <div class=\"page-bottom\">");
                writer.WriteLine("                @Html.LinkVoltarAListagem()");
                writer.WriteLine("                <button type=\"submit\" class=\"btn btn-success\"><i class=\"glyphicon glyphicon-save\"></i> Salvar</button>");
                writer.WriteLine("            </div>");
                writer.WriteLine("        </div>");
                writer.WriteLine("    }");
                writer.WriteLine("</div>");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view edit para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraEdit(string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\Edit.cshtml");

                writer.WriteLine("@model " + model + "ViewModel");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"inbox-head bg-blue\">");
                writer.WriteLine("    <h3><i class=\"glyphicon glyphicon-pencil\"></i> Editar " + model + "</h3>");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"form-horizontal\">");
                writer.WriteLine("    @using (Html.BeginForm())");
                writer.WriteLine("    {");
                writer.WriteLine("        @Html.Partial(\"_CamposFormulario\")");
                writer.WriteLine("");
                writer.WriteLine("        <div class=\"col-lg-12 well\">");
                writer.WriteLine("            <div class=\"page-bottom\">");
                writer.WriteLine("                @Html.LinkVoltarAListagem()");
                writer.WriteLine("                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-pencil\"></i> Salvar</button>");
                writer.WriteLine("            </div>");
                writer.WriteLine("        </div>");
                writer.WriteLine("    }");
                writer.WriteLine("</div>");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view details para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraDetails(string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\Details.cshtml");

                writer.WriteLine("@model " + model + "");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"inbox-head bg-cyan\">");
                writer.WriteLine("    <h3><i class=\"glyphicon glyphicon-search\"></i> Detalhes do " + model + "</h3>");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"form-horizontal\">");
                writer.WriteLine("    @using (Html.BeginForm())");
                writer.WriteLine("    {");
                writer.WriteLine("        @Html.Partial(\"_CamposDetalhe\")");
                writer.WriteLine("");
                writer.WriteLine("        <div class=\"col-lg-12 well\">");
                writer.WriteLine("            <div class=\"page-bottom\">");
                writer.WriteLine("                @Html.LinkVoltarAListagem()");
                writer.WriteLine("                @Html.LinkEditar(Model.id)");
                writer.WriteLine("            </div>");
                writer.WriteLine("        </div>");
                writer.WriteLine("    }");
                writer.WriteLine("</div>");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view delete para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string geraDelete(string model)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\Delete.cshtml");

                writer.WriteLine("@model " + model + "");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"inbox-head bg-red\">");
                writer.WriteLine("    <h3><i class=\"glyphicon glyphicon-trash\"></i> Excluir " + model + "?</h3>");
                writer.WriteLine("</div>");
                writer.WriteLine("");
                writer.WriteLine("<div class=\"form-horizontal\">");
                writer.WriteLine("    @using (Html.BeginForm())");
                writer.WriteLine("    {");
                writer.WriteLine("        @Html.Partial(\"_CamposDetalhe\")");
                writer.WriteLine("");
                writer.WriteLine("        <div class=\"col-lg-12 well\">");
                writer.WriteLine("            <div class=\"page-bottom\">");
                writer.WriteLine("                @Html.LinkVoltarAListagem()");
                writer.WriteLine("                <button type=\"submit\" class=\"btn btn-danger\"><i class=\"glyphicon glyphicon-trash\"></i> Excluir</button>");
                writer.WriteLine("            </div>");
                writer.WriteLine("        </div>");
                writer.WriteLine("    }");
                writer.WriteLine("</div>");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view Campos Detalhe para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lstProperties"></param>
        /// <returns></returns>
        private string gera_CamposDetalhe(string model, List<ModelRegister> lstProperties)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\_CamposDetalhe.cshtml");

                writer.WriteLine("@model " + model + "");
                writer.WriteLine("");
                writer.WriteLine("<section class=\"Panel\">");
                writer.WriteLine("    <header class=\"panel-heading\"> " + model + "</header>");
                writer.WriteLine("    <div class=\"panel-body\">");

                foreach (var reg in lstProperties)
                {
                    if (reg.Tipo.ToLower() != "string" && reg.Tipo.Substring(0, 3).ToLower() != "int" && reg.Tipo.ToLower() != "decimal" && reg.Tipo.ToLower() != "double" && reg.Tipo.ToLower() != "float" && reg.Tipo.ToLower() != "datetime")
                    {
                        writer.WriteLine("        <div class=\"form-group\">");
                        writer.WriteLine("            @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("            <div class=\"col-lg-6\">");
                        writer.WriteLine("                <label class=\"text-detail\">@Model." + reg.Nome + " </label>");
                        writer.WriteLine("            </div>");
                        writer.WriteLine("        </div>");
                    }
                    else
                    {
                        writer.WriteLine("        <div class=\"form-group\">");
                        writer.WriteLine("            @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("            <div class=\"col-lg-6\">");
                        writer.WriteLine("                <label class=\"text-detail\">@Model." + reg.Nome + " </label>");
                        writer.WriteLine("            </div>");
                        writer.WriteLine("        </div>");
                    }
                }

                writer.WriteLine("    </div>");
                writer.WriteLine("</section>");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }

        /// <summary>
        /// gera a view campos formulario para o modelo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lstProperties"></param>
        /// <returns></returns>
        private string gera_CamposFormulario(string model, List<ModelRegister> lstProperties)
        {
            try
            {
                //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                StreamWriter writer = new StreamWriter(folder + @"\_CamposFormulario.cshtml");

                writer.WriteLine("@model " + model + "ViewModel");
                writer.WriteLine("");
                writer.WriteLine("<section class=\"Panel\">");
                writer.WriteLine("    <header class=\"panel-heading\"> " + model + "</header>");
                writer.WriteLine("    <div class=\"panel-body\">");
                writer.WriteLine("        <div class=\"col-md-12\">");

                foreach (var reg in lstProperties)
                {
                    writer.WriteLine("            <div class=\"form-group\">");
                    if (reg.Tipo == "DateTime")
                    {
                        writer.WriteLine("                @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("                <div class=\"col-lg-3\" data-behaviour=\"datepicker\">");
                        writer.WriteLine("                    @Html.TextBoxFor(model => model." + reg.Nome + ", new { Value = Model." + reg.Nome + ".ToString(\"dd/MM/yyyy\"), @class = \"form-control\" })");
                        writer.WriteLine("                    <span class=\"input-group-btn\"><button class=\"btn btn-default\" type=\"button\"><i class=\"glyphicon glyphicon-calendar\"></i></button></span>");
                        writer.WriteLine("                </div>");

                        writer.WriteLine("                <div class=\"validation-sign\">");
                        writer.WriteLine("                    @Html.ValidationMessageFor(model => model." + reg.Nome + ")");
                        writer.WriteLine("                </div>");
                    }
                    else if (reg.Tipo.ToLower() == "bool" || reg.Tipo.ToLower() == "boolean")
                    {
                        writer.WriteLine("                @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("                <div class=\"col-lg-4\">");
                        writer.WriteLine("                <label>");
                        writer.WriteLine("                    @Html.CheckBoxFor(model => model." + reg.Nome + ", new { @class = \"form-control\" })");
                        writer.WriteLine("                </label>");
                        writer.WriteLine("                </div>");
                        writer.WriteLine("                <div class=\"validation-sign\">");
                        writer.WriteLine("                    @Html.ValidationMessageFor(model => model." + reg.Nome + ")");
                        writer.WriteLine("                </div>");
                    }
                    else if (reg.Tipo.ToLower() == "string" || reg.Tipo.Substring(0, 3).ToLower() == "int" || reg.Tipo.ToLower() == "decimal" || reg.Tipo.ToLower() == "double" || reg.Tipo.ToLower() == "float")
                    {
                        
                        writer.WriteLine("                @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("                <div class=\"col-lg-4\">");
                        writer.WriteLine("                    @Html.TextBoxFor(model => model." + reg.Nome + ", new { @class = \"form-control\" })");
                        writer.WriteLine("                </div>");
                        writer.WriteLine("                <div class=\"validation-sign\">");
                        writer.WriteLine("                    @Html.ValidationMessageFor(model => model." + reg.Nome + ")");
                        writer.WriteLine("                </div>");
                        
                    } else //então deve ser um navigation, vou tentar fazer um dropdown
                    {

                        writer.WriteLine("                @Html.LabelFor(model => model." + reg.Nome + ", new { @class = \"col-lg-2 control-label\" })");
                        writer.WriteLine("                <div class=\"col-lg-4\">");
                        writer.WriteLine("                    @Html.DropDownListFor(model => model.id" + reg.Tipo + ", Model." + reg.Tipo + ", String.Empty, new { @class = \"form-control\", data_behaviour = \"select2\", data_placeholder = \"Escolha uma opção\" })");
                        writer.WriteLine("                </div>");
                    }
                    writer.WriteLine("            </div>");
                }

                writer.WriteLine("");
                writer.WriteLine("        </div>");
                writer.WriteLine("    </div>");
                writer.WriteLine("</section>");
                writer.WriteLine("");
                writer.WriteLine("@Html.HiddenFor(model => model.id)");
                writer.WriteLine("@Html.ValidationMessageFor(model => model.id)");

                //Fechando o arquivo
                writer.Close();
                //Limpando a referencia dele da memória
                writer.Dispose();

                return "True";
            }
            catch (Exception)
            {

                return "False";
            }
        }


    }
}
