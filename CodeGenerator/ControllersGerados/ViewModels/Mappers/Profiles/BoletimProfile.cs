using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class BoletimProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Boletim, BoletimViewModel>();
                .ForMember(d => d.DadosColeta, x => x.Ignore());
                .ForMember(d => d.Responsavel, x => x.Ignore());
                .ForMember(d => d.Responsavel, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.ICollection<Conclusao>, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.Manancial, x => x.Ignore());

            Mapper.CreateMap<BoletimViewModel, Boletim>();
                .ForMember(d => d.DadosColeta, x => x.Ignore());
                .ForMember(d => d.Responsavel, x => x.Ignore());
                .ForMember(d => d.Responsavel, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.ICollection<Conclusao>, x => x.Ignore());
                .ForMember(d => d.Boolean, x => x.Ignore());
                .ForMember(d => d.Manancial, x => x.Ignore());
        }
    }
}
